<?php namespace App\Controllers;

use App\Models\CatModel;
use App\Models\ItemModel;
use App\Models\CartModel;
use App\Models\AdminModel;

class Admin extends BaseController
{
    private $catModel = null;
    private $itemModel = null;
    private $cartModel = null;
    private $adminModel = null;

    function __construct()
    {
        $this->catModel = new CatModel();
        $this->itemModel = new ItemModel();
        $this->cartModel = new CartModel();
        $this->adminModel = new AdminModel();
    }


    public function index()
    {
        if (!isset($_SESSION['adminname'])) {
            return redirect()->to(site_url('adminlogin'));
        }

        $data['categories'] = $this->catModel->getCategories();
        $data['titlecat'] = 'Categories:';
        echo view('templates/header_admin.php');
        echo '<h1>Welcome to view, edit or delete your item categories.</h1><br>';
        echo view('admin/cat', $data);
        echo view('templates/footer_admin');
    }


    public function admins()
    {
        $data['adminlogins'] = $this->adminModel->getAdminlist();
        echo view('templates/header_admin.php');
        echo '<h1>Welcome to view, edit or delete your site admins.</h1><br>';
        echo view('admin/adminlist',$data);
        echo view('templates/footer_admin');
    }


    public function remove($id) {
        $this->adminModel->remove($id);
        return redirect()->to(site_url('/admin/admins'));
    }


    public function saveCat($category_id = null) {
        if ($this->request->getMethod() === 'post') {
            if (!$this->validate([
                'catname' => 'required|max_length[100]',
                'catimage' => 'max_length[100]'
            ])) {
                $data['id'] = $this->request->getPost('id');
                $data['catname'] = $this->request->getPost('catname');
                $data['catimage'] = $this->request->getPost('catimage');
                $this->showForm($data);
            }
            else {
                $save['id'] = $this->request->getPost('id');
                $save['catname'] = $this->request->getPost('catname');
                $save['catimage'] = $this->request->getPost('catimage');
                $this->catModel->save($save);
                return redirect('admin/index');
            }
        }
        else {
        $data['id'] = '';
        $data['catname'] = '';
        $data['catimage'] = '';
        if ($category_id != null) {
            $category = $this->catModel->get($category_id);
            $data['id'] = $category['id'];
            $data['catname'] = $category['catname'];
            $data['catimage'] = $category['catimage'];
        }
        $this->showForm($data);
        }
    }


    private function showForm($data) {
        $data['title'] = 'Categories';
        echo view('templates/header_admin.php');
        echo view('admin/create_cat.php',$data);
        echo view('templates/footer_admin.php');
    }


    public function removecat($id) {
        try {
            $this->itemModel->removeByCategory($id);
            $this->catModel->remove($id);
            return redirect ('admin/index');
        }
        catch (\Exception $e) {
            if ($e->getCode() === 1451) {
                $data['title'] = 'Cannot remove item category.';
                $data['message'] = "You can't remove item category because there is/are order(s) for them. Please remove order(s) first.";
                echo view('templates/header_admin.php');
                echo view('admin/notification.php',$data);
                echo view('templates/footer_admin.php');
            }
            else {
                throw new $e;
            }
        }
    }

}