<?php namespace App\Controllers;

use App\Models\CatModel;
use App\Models\ItemModel;
use App\Models\CartModel;

class Item extends BaseController
{
    private $catModel = null;
    private $itemModel = null;

    function __construct()
    {
        $this->catModel = new CatModel();
        $this->itemModel = new ItemModel();
        $this->cartModel = new CartModel();
    }


    public function index()
    {
        $data['categories'] = $this->catModel->getCategories();
        $data['items'] = $this->itemModel->getItemsAdmin();
        $data['cart_amount'] = $this->cartModel->amount();
        $data['titleitem'] = 'Items:';
        echo view('templates/header_admin.php');
        echo '<h1>Welcome to view and/or delete your items.</h1><br>';
        echo view('admin/item', $data);
        echo view('templates/footer_admin');
    }


    public function saveitem($item_id = null) {
        if ($this->request->getMethod() === 'post') {
            if (!$this->validate([
                'item' => 'required|max_length[100]',
                'price' => 'required|max_length[100]',
                'category_id' => 'required|max_length[100]',
                'instore' => 'max_length[100]',
                'description' => 'max_length[100]',
                'image' => 'max_length[100]'
            ])) {
                $data['id'] = $this->request->getPost('id');
                $data['item'] = $this->request->getPost('item');
                $data['price'] = $this->request->getPost('price');
                $data['category_id'] = $this->request->getPost('category_id');
                $data['instore'] = $this->request->getPost('instore');
                $data['description'] = $this->request->getPost('description');
                $data['image'] = $this->request->getPost('image');
                $this->showForm($data);
            }
            else {
                $save['id'] = $this->request->getPost('id');
                $save['item'] = $this->request->getPost('item');
                $save['price'] = $this->request->getPost('price');
                $save['category_id'] = $this->request->getPost('category_id');
                $save['instore'] = $this->request->getPost('instore');
                $save['description'] = $this->request->getPost('description');
                $save['image'] = $this->request->getPost('image');
                $this->itemModel->save($save);
                return redirect('item/index');
            }
        }
        else {
        $data['id'] = '';
        $data['item'] = '';
        $data['price'] = '0';
        $data['category_id'] = '';
        $data['instore'] = '0';
        $data['description'] = '';
        $data['image'] = '';
        if ($item_id != null) {
            $item = $this->itemModel->getWithID($item_id);
            $data['id'] = $item['id'];
            $data['item'] = $item['item'];
            $data['price'] = $item['price'];
            $data['category_id'] = $item['category_id'];
            $data['instore'] = $item['instore'];
            $data['description'] = $item['description'];
            $data['image'] = $item['image'];
        }
        $this->showForm($data);
        }
    }


    private function showForm($data) {
        $data['title'] = 'Items';
        echo view('templates/header_admin.php');
        echo view('admin/create_item.php',$data);
        echo view('templates/footer_admin.php');
    }


    public function removeitem($id) {
        try {
            $this->itemModel->remove($id);
            return redirect ('item/index');
        }
        catch (\Exception $e) {
            if ($e->getCode() === 1451) {
                $data['title'] = 'Cannot remove item.';
                $data['message'] = "You can't remove the item because there is/are order(s) for them. Please remove order(s) first if you still want to remove the item.";
                echo view('templates/header_admin.php');
                echo view('admin/notification.php',$data);
                echo view('templates/footer_admin.php');
            }
            else {
                throw new $e;
            }
        }
    }

}