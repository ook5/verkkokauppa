<?php namespace App\Controllers;

use App\Models\CatModel;
use App\Models\CartModel;

class Aboutus extends BaseController
{
	private $catModel = null;

	function __construct()
	{
		$this->catModel = new CatModel();
		$this->cartModel = new CartModel();
	}


	public function index()
	{
		$data['categories'] = $this->catModel->getCategories();
		$data['cart_amount'] = $this->cartModel->amount();
		echo view('templates/header',$data);
		echo view('aboutus');
		echo view('templates/footer');
	}

	//--------------------------------------------------------------------

}