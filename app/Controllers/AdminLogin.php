<?php

namespace App\Controllers;

use App\Models\AdminLoginModel;
use App\Models\CatModel;
use App\Models\CartModel;

class AdminLogin extends BaseController {

    /* Constructor starts session. */
    public function __construct() {
        $session = \Config\Services::session();
        $session->start();

        $this->catModel = new CatModel();
        $this->cartModel = new CartModel();
    }

    public function index() {
        $data['categories'] = $this->catModel->getCategories();
        $data['cart_amount'] = $this->cartModel->amount();
        echo view('templates/header',$data);
        echo view('login/adminlogin');
        echo view('templates/footer');
    }

    public function logout() {
        $data['categories'] = $this->catModel->getCategories();
        $data['cart_amount'] = $this->cartModel->amount();
        unset($_SESSION['adminname']);
        echo view('templates/header',$data);
        echo view('frontpage');
        echo view('templates/footer');
    }

    public function adminRegister() {
        $data['categories'] = $this->catModel->getCategories();
        $data['cart_amount'] = $this->cartModel->amount();
        echo view('templates/header_admin',$data);
        echo view('login/adminregister');
        echo view('templates/footer_admin');
    }

    public function adminRegistration() {

        $model = new AdminLoginModel();
        
        if (!$this->validate([
            'adminname' => 'required|min_length[8]|max_length[100]',
            'firstname' => 'required|min_length[3]|max_length[100]',
            'lastname' => 'required|min_length[3]|max_length[100]',
            'password' => 'required|min_length[8]|max_length[255]',
            'confirmpassword' => 'required|min_length[8]|max_length[255]|matches[password]'
        ])){
            $data['categories'] = $this->catModel->getCategories();
            $data['cart_amount'] = $this->cartModel->amount();
            echo view('templates/header_admin',$data);
            echo view('login/adminregister');
            echo view('templates/footer_admin');
        }
        else {
            $model->save([
                'id' => $this->request->getPost('id'),     
                'adminname' => $this->request->getPost('adminname'),
                'firstname' => $this->request->getPost('firstname'),
                'lastname' => $this->request->getPost('lastname'),
                'password' => password_hash($this->request->getPost('password'),PASSWORD_DEFAULT)
            ]);
            return redirect()->to(site_url('/admin/admins'));
        }
    }

    public function check() {
        
        $model = new AdminLoginModel();

        if (!$this->validate([
            'adminname' => 'required|min_length[8]|max_length[50]',
            'password' => 'required|min_length[8]|max_length[255]',
        ])){
            $data['categories'] = $this->catModel->getCategories();
            $data['cart_amount'] = $this->cartModel->amount();
            echo view('templates/header', $data);
            echo view('login/adminlogin');
            echo view('templates/footer');
        }
        else {
            $user = $model->check( // Use model to check if user exists.
                $this->request->getPost('adminname'),
                $this->request->getPost('password')
            );
            if ($user) { // If there is user, store into session and redirect to todo.
                $_SESSION['adminname'] = $user;
                return redirect()->to(base_url('/admin/index')); 
            }
            else { // User is null, redirect to login page.
                $data['categories'] = $this->catModel->getCategories();
                $data['cart_amount'] = $this->cartModel->amount();
                echo view('templates/header', $data);
                print "<p>Admin name or password doesn't match</p>";
                echo view('login/adminlogin');        
                echo view('templates/footer');
            }
        }
    }

}