<?php  namespace App\Controllers;

use App\Models\CatModel;
use App\Models\CartModel;


class Cart extends BaseController
{
    private $catModel = null;
    private $cartModel = null;

    public function __construct() {
        $this->catModel = new CatModel();
        $this->cartModel = new CartModel();
        
	}


    public function index()
	{
        $data['categories'] = $this->catModel->getCategories();
        $data['items'] = $this->cartModel->cart();
        $data['cart_amount'] = $this->cartModel->amount();
            if (isset($_SESSION['user'])) { 
                $data['customer'] =  $_SESSION['user']; 
                }
            echo view('templates/header',$data );
            echo view('cart',$data);
            echo view('templates/footer');
	}


	public function orders()
	{
        //var_dump(isset($_SESSION['user']) ? $_SESSION['user']->id : 0);
        //exit;
        $customer = [
            'id' => isset($_SESSION['user']) ? $_SESSION['user']->id : 0,
            'firstname' => $this->request->getPost('firstname'),
            'lastname' => $this->request->getPost('lastname'),
            'address' => $this->request->getPost('address'),
            'postalcode' => $this->request->getPost('postalcode'),
            'city' => $this->request->getPost('city'),
            'phone' => $this->request->getPost('phone'),
            'email' => isset($_SESSION['user']) ? $_SESSION['user']->email : '',
            'password' => isset($_SESSION['user']) ? $_SESSION['user']->password : ''
        ];

		$this->cartModel->orders($customer);

        $data['categories'] = $this->catModel->getCategories();
		$data['cart_amount'] = $this->cartModel->amount();
        echo view('templates/header',$data);
		echo view('thankyou');
		echo view('templates/footer');
	}


    public function add($item_id) { 
		$this->cartModel->add($item_id);
        return redirect()->to(site_url('/store/item/' . $item_id));
    }


    public function clear() {
		$this->cartModel->clear();
        return redirect()->to(site_url('cart/index'));		
	}


	public function remove($item_id) {
		$this->cartModel->remove($item_id);
		return redirect()->to(site_url('cart/index'));	
	}


}