<?php namespace App\Controllers;

use App\Models\CustomerModel;
use App\Models\OrdersModel;
use App\Models\OrdertableModel;

class Orders extends BaseController
{
    private $customerModel = null;
    private $ordersModel = null;
    private $ordertableModel = null;

    function __construct()
    {
        $this->customerModel = new CustomerModel();
        $this->ordersModel = new OrdersModel();
        $this->ordertableModel = new OrdertableModel();
    }


    public function index() {
        $data['allorders'] = $this->ordersModel->getOrders();
        echo view('templates/header_admin.php');
        echo view('admin/orders.php',$data);
        echo view('templates/footer_admin.php');
    }


    public function remove($id) {
        $this->ordertableModel->removeByOrder($id);
        $this->ordersModel->remove($id);
        return redirect()->to(site_url('/orders/index'));
    }
}