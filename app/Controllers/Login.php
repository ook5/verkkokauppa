<?php

namespace App\Controllers;

use App\Models\LoginModel;
use App\Models\CatModel;
use App\Models\CartModel;

class Login extends BaseController {

    /* Constructor starts session. */
    public function __construct() {
        $session = \Config\Services::session();
        $session->start();
        
        $this->catModel = new CatModel();
        $this->cartModel = new CartModel();
    }

    public function index() {
        $data['categories'] = $this->catModel->getCategories();
        $data['cart_amount'] = $this->cartModel->amount();
        echo view('templates/header',$data);
        echo view('login/login');
        echo view('templates/footer');
    }

    public function logout() {
        $data['categories'] = $this->catModel->getCategories();
        $data['cart_amount'] = $this->cartModel->amount();
        unset($_SESSION['user']);
        echo view('templates/header',$data);
        echo view('frontpage');
        echo view('templates/footer');
    }

    public function register() {
        $data['categories'] = $this->catModel->getCategories();
        $data['cart_amount'] = $this->cartModel->amount();
        echo view('templates/header',$data);
        echo view('login/register');
        echo view('templates/footer');
    }

    public function registration() {
        
        $model = new LoginModel();
        
        if (!$this->validate([
            'firstname' => 'required|min_length[3]|max_length[50]',
            'lastname' => 'required|min_length[3]|max_length[50]',
            'address' => 'required|min_length[5]|max_length[100]',
            'postalcode' => 'required|min_length[5]|max_length[5]',
            'city' => 'required|min_length[2]|max_length[50]',
            'phone' => 'min_length[5]|max_length[25]',
            'email' => 'required|min_length[5]|max_length[100]',
            'password' => 'required|min_length[8]|max_length[255]',
            'confirmpassword' => 'required|min_length[8]|max_length[255]|matches[password]'
        ])){
            $data['categories'] = $this->catModel->getCategories();
            $data['cart_amount'] = $this->cartModel->amount();
            echo view('templates/header',$data);
            echo view('login/register');
            echo view('templates/footer');
        }
        else {
            $model->save([
                'id' => $this->request->getPost('id'),
                'firstname' => $this->request->getPost('firstname'),
                'lastname' => $this->request->getPost('lastname'),
                'address' => $this->request->getPost('address'),
                'postalcode' => $this->request->getPost('postalcode'),
                'city' => $this->request->getPost('city'),
                'phone' => $this->request->getPost('phone'),
                'email' => $this->request->getPost('email'),
                'password' => password_hash($this->request->getPost('password'),PASSWORD_DEFAULT)
            ]);
            $data['categories'] = $this->catModel->getCategories();
            $data['cart_amount'] = $this->cartModel->amount();
            echo view('templates/header',$data);
            echo view('thankreg');
            echo view('login/login');
            echo view('templates/footer');
        }
    }

    public function check() {
        $model = new LoginModel();
        
        if (!$this->validate([
            'email' => 'required|min_length[8]|max_length[50]',
            'password' => 'required|min_length[8]|max_length[255]',
        ])){
            $data['categories'] = $this->catModel->getCategories();
            $data['cart_amount'] = $this->cartModel->amount();
            echo view('templates/header', $data);
            echo view('login/login');
            echo view('templates/footer');
        }
        else {
            $user = $model->check( // Use model to check if user exists.
                $this->request->getPost('email'),
                $this->request->getPost('password')
            );
            if ($user) { // If there is user, store into session and redirect to index.
                $_SESSION['user'] = $user;
                return redirect()->to(base_url(''));
            } else { // User is null, redirect to login page.
                $data['categories'] = $this->catModel->getCategories();
                $data['cart_amount'] = $this->cartModel->amount();
                echo view('templates/header', $data);
                print "<p>Email or password doesn't match</p>";
                echo view('login/login');
                echo view('templates/footer');
            }
        }
    }

}