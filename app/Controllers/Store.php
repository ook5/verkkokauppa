<?php namespace App\Controllers;

use App\Models\CatModel;
use App\Models\ItemModel;
use App\Models\CartModel;

class Store extends BaseController
{
    private $catModel=null;
	private $itemModel=null;

	function __construct()
    {
        $this->catModel = new CatModel();
		$this->itemModel = new ItemModel();
		$this->cartModel = new CartModel();
    }


	public function index($category_id)
	{
        $data['categories'] = $this->catModel->getCategories();
		$data['items'] = $this->itemModel->getWithCategory($category_id);
		$data['cart_amount'] = $this->cartModel->amount();
		echo view('templates/header',$data);
		echo view('store');
		echo view('templates/footer');
	}


    public function item($item_id) {
        $data['categories'] = $this->catModel->getCategories();
		$data['item'] = $this->itemModel->getItem($item_id);
		$data['cart_amount'] = $this->cartModel->amount();
		echo view('templates/header',$data);
		echo view('item',$data);
		echo view('templates/footer');
    }

	//--------------------------------------------------------------------

}
