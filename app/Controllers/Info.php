<?php

namespace App\Controllers;

use App\Models\CatModel;
use App\Models\CartModel;
use App\Models\OrdersModel;
use App\Models\LoginModel;
use App\Models\InfoModel;
use App\Models\OrdertableModel;
use App\Models\ItemModel;
use App\Models\CustomerModel;

class Info extends BaseController
{
	private $catModel = null;
	private $customerModel = null;

	function __construct()
	{
		$session = \Config\Services::session();
		$session->start();

		$this->catModel = new CatModel();
		$this->cartModel = new CartModel();
		$this->ordersModel = new OrdersModel();
		$this->infoModel = new InfoModel();
		$this->loginModel = new LoginModel();
		$this->customerModel = new CustomerModel();
	}

	public function index()
	{
		$data['categories'] = $this->catModel->getCategories();
		$data['cart_amount'] = $this->cartModel->amount();
		$data['allorders'] = $this->ordersModel->getOrders();
		//$data['customer'] = $this->infoModel->find($_SESSION['user']);
		$data['customer'] =  $_SESSION['user'];
		//var_dump($data['customer']);
		//exit;
		echo view('templates/header', $data);
		//print_r ($data);
		echo view('info', $data);
		echo view('templates/footer');
	}

	public function editInfo($customer_id = null)
	{
		
		$data['categories'] = $this->catModel->getCategories();
		$data['cart_amount'] = $this->cartModel->amount();
		$data['allorders'] = $this->ordersModel->getOrders();	
		$data['customer'] =  $_SESSION['user'];

		echo view('templates/header', $data);
		echo view('editInfo', $data);
		echo view('templates/footer');
		if ($this->request->getMethod() === 'post') {
			if (!$this->validate([
				'firstname' => 'required|min_length[3]|max_length[50]',
				'lastname' => 'required|min_length[3]|max_length[50]',
				'address' => 'required|min_length[5]|max_length[100]',
				'postalcode' => 'required|min_length[5]|max_length[5]',
				'city' => 'required|min_length[2]|max_length[50]',
				'phone' => 'min_length[5]|max_length[25]',
				'email' => 'required|min_length[5]|max_length[100]',
				'password' => 'required|min_length[8]|max_length[255]',
				'confirmpassword' => 'required|min_length[8]|max_length[255]|matches[password]'
			])) {
				$data['id'] = $this->request->getPost('id');
				$data['firstname'] = $this->request->getPost('firstname');
				$data['lastname'] = $this->request->getPost('lastname');
				$data['address'] = $this->request->getPost('address');
				$data['postalcode'] = $this->request->getPost('postalcode');
				$data['city'] = $this->request->getPost('city');
				$data['phone'] = $this->request->getPost('phone');
				$data['email'] = $this->request->getPost('email');
				$data['password'] = $this->request->getPost('password');
				$data['confirmpassword'] = $this->request->getPost('confirmpassword');
				//$this->showForm($data);
			} else {
				$save['id'] = $this->request->getPost('id');
				$save['firstname'] = $this->request->getPost('firstname');
				$save['lastname'] = $this->request->getPost('lastname');
				$save['address'] = $this->request->getPost('address');
				$save['postalcode'] = $this->request->getPost('postalcode');
				$save['city'] = $this->request->getPost('city');
				$save['phone'] = $this->request->getPost('phone');
				$save['email'] = $this->request->getPost('email');
				$save['password'] = password_hash($this->request->getPost('password'),PASSWORD_DEFAULT);
				$save['confirmpassword'] = password_hash($this->request->getPost('confirmpassword'),PASSWORD_DEFAULT);
				$this->infoModel->save($save);
				$_SESSION['user'] = $this->customerModel->getWithID($save['id']);
				//var_dump ($this->customerModel->get($save['id']));
				//var_dump($_SESSION['user']);
				//var_dump($save['id']);
				//exit;
				return redirect()->to(site_url('info'));
			}
		} else {
			$data['id'] = '';
			$data['firstname'] = '';
			$data['lastname'] = '';
			$data['address'] = '';
			$data['city'] = '';
			$data['phone'] = '';
			$data['email'] = '';
			$data['password'] = '';
			$data['confirmpassword'] = '';
			if ($customer_id != null) {
				$customer = $this->infoModel->getWithID($customer_id);
				$data['id'] = $customer['id'];
				$data['firstname'] = $customer['firstname'];
				$data['lastname'] = $customer['lastname'];
				$data['address'] = $customer['address'];
				$data['city'] = $customer['city'];
				$data['phone'] = $customer['phone'];
				$data['email'] = $customer['email'];
				$data['password'] = $customer['password'];
				$data['confirmpassword'] = $customer['confirmpassword'];
			}
			//$this->showForm($data);
		}
	}

/*
	private function showForm($data)
	{
		$data['title'] = 'Items';
		echo view('templates/header.php', $data);
		echo view('editInfo', $data);
		echo view('templates/footer.php');
	}
*/
}
