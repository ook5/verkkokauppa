drop database if exists webstore;

create database webstore;

use webstore;

create table customer (
    id int primary key auto_increment,
    email varchar(100),
    firstname varchar(50) not null,
    lastname varchar(50) not null,
    address varchar(100) not null,
    postalcode varchar(5) not null,
    city varchar(50) not null,
    phone varchar(25),
    password varchar(255)
);

create table adminlogin (
    id int primary key auto_increment,
    adminname varchar(50) not null,
    firstname varchar(50) not null,
    lastname varchar(50) not null,
    password varchar(255) not null
);

create table category (
    id int primary key auto_increment,
    catname varchar(100) not null,
    catimage varchar(100)
);

create table item (
    id int primary key auto_increment,
    item varchar(100) not null,
    price int not null,
    category_id int not null,
    index(category_id),
    instore int,
    description text,
    image varchar(100),
    foreign key (category_id) references category(id)
    on delete restrict
);

create table orders (
    id int primary key auto_increment,
    time timestamp default current_timestamp,
    status varchar(10),
    customer_id int not null,
    index(customer_id),
    foreign key (customer_id) references customer(id)
    on delete restrict
);

create table ordertable (
    orders_id int not null,
    index orders_id(orders_id),
    foreign key (orders_id) references orders(id)
    on delete restrict,
    item_id int not null,
    index item_id(item_id),
    foreign key (item_id) references item(id)
    on delete restrict,
    amount int not null
);