/* Catecory inserts */
insert into category (catname, catimage) values ('Shirts','shirts_cat.png');
insert into category (catname, catimage) values ('Pants','pants_cat.png');
insert into category (catname, catimage) values ('Hats','hats_cat.png');

/* Item inserts*/
insert into item (item, price, category_id, instore, description, image) values ('Janne fleece',99,1,35,'Cool green fleece sweater made of 100% recycled pöly-Esteri.','shirt01.png');
insert into item (item, price, category_id, instore, description, image) values ('AK hoodie',99,1,30,'Wear this hooded sweatshirt for example while you play Pukemon Go on some dark alley. 100% organic cotton.','shirt02.png');
insert into item (item, price, category_id, instore, description, image) values ('Näkkileipä sweater',75,1,25,'It is black, it is warm and it is 100% organic cotton.','shirt03.png');
insert into item (item, price, category_id, instore, description, image) values ('Mathletes sweater',64,1,15,'Well this is based on reality. Wear this while doing some algebra. 100% organic cotton.','shirt04.png');
insert into item (item, price, category_id, instore, description, image) values ('Dark matter sweater',49,1,5,'Nice lightweight sweater made of 100% cotton.','shirt05.png');

insert into item (item, price, category_id, instore, description, image) values ('Erkki jeans',99,2,10,'Who would not want to wear white jeans? Grab a pair of these and pretend it is summer. 100% organic cotton.','pants01.png');
insert into item (item, price, category_id, instore, description, image) values ('Homebound college pants',79,2,15,'These college pants will look awesome on you no matter where you go. 100% organic cotton.','pants02.png');
insert into item (item, price, category_id, instore, description, image) values ('Static college pants',64,2,5,'You could wear these pants while watching static noise from television. 50% organic cotton, 50% recycled polyester.','pants03.png');
insert into item (item, price, category_id, instore, description, image) values ('Vili-Petteri jeans',99,2,2,'Black jeans, a wardrobe staple, made of 100% organic cotton.','pants04.png');

insert into item (item, price, category_id, instore, description, image) values ('Boomer hat',50,3,5,'100% wool hat made in Peru by llama farmers. Practical ear flaps for cold days in the mountains.','hat01.png');
insert into item (item, price, category_id, instore, description, image) values ('Baseball cap',45,3,5,'OOK official baseball cap for our loyal fans. 100% cotton.', 'hat02.png');
insert into item (item, price, category_id, instore, description, image) values ('Viljo wool beanie',40,3,5,'This 100% wool beanie will keep your head warm whether you go shopping or exploring the great outdoors.','hat03.png');
insert into item (item, price, category_id, instore, description, image) values ('Roni snapback',35,3,5,'We got some of these snapbacks which are handmade by Peter Woodhead, only 25 pieces made.','hat04.png');

/* Customer inserts | Password is rootroot*/
insert into customer (id, firstname, lastname, address, postalcode, city, phone, email, password) values (1, 'Pannes', 'Pörviölä', 'Kirkkokatu 6 b 666', '90120', 'Oulu', '555-1234567', 'pannesp@jeemail.com', '$2y$10$FxoqKrCPmIMF8iMYCvmdOeVOXEHXE1.qEH1jkyLF6NzUbpEiw0Iv6');
insert into customer (id, firstname, lastname, address, postalcode, city, phone, email, password) values (2, 'Pantti', 'Porkkala', 'Untuvakuja 1 c 4', '90250', 'Oulu', '555-7654321', 'panttip@jeemail.com', '$2y$10$FxoqKrCPmIMF8iMYCvmdOeVOXEHXE1.qEH1jkyLF6NzUbpEiw0Iv6');
insert into customer (id, firstname, lastname, address, postalcode, city, phone, email, password) values (3, 'Puhis-Jekkis', 'Pongerinen', 'Torikatu 61 d 82', '90120', 'Oulu', '555-1122334', 'puhisjekkisp@jeemail.com', '$2y$10$FxoqKrCPmIMF8iMYCvmdOeVOXEHXE1.qEH1jkyLF6NzUbpEiw0Iv6');

/* Adminlogin inserts | Password is rootroot*/
insert into adminlogin (id, adminname, firstname, lastname, password) values (1, 'adminadmin', 'Root', 'Root', '$2y$10$FxoqKrCPmIMF8iMYCvmdOeVOXEHXE1.qEH1jkyLF6NzUbpEiw0Iv6');