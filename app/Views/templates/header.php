<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
    <link rel="stylesheet" href="<?= base_url('/css/style.css'); ?>">
    <title>OOK-Clothing</title>
    <!-- Navigation starts -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-dark">
        <a class="navbar-brand frontpagelogo" href="<?= site_url('Home/index/') ?>">OOK-Clothing</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Categories
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <?php foreach ($categories as $category) : ?>
                            <a class="dropdown-item" href="<?= site_url('store/index/' . $category['id']) ?>"><?= $category['catname'] ?></a>
                        <?php endforeach; ?>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= site_url('/aboutus/') ?>">About us</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <?php 
                if (isset($_SESSION['user'])) {
                ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?= site_url('login/logout/') ?>">Logout</a>
                </li>
                <?php
                } else {
                ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?= site_url('login/') ?>">Login</a>
                </li>
                <?php
                }
                ?>
                <?php
                if (isset($_SESSION['user'])) {
                ?>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= site_url('info/') ?>">
                            <i class="fa fa-user-circle" aria-hidden="true"></i>
                            Account info</a>
                    </li>
                <?php
                }
                ?>
                <li class="nav-item">
                    <a id="cart" class="nav-link" href="<?= site_url('cart/index'); ?>">
                        <i class="fas fa-shopping-cart fa-lg cartadded" id="shoppingcart" title="Shopping cart">
                        </i>
                        <span><?= $cart_amount ?></span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- Navigation ends -->
</head>

<body class="d-flex flex-column min-vh-100">
    <div class="container">