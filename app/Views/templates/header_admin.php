<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= base_url('/css/adminstyles.css'); ?>">
    <title>OOK-Clothing Admin site</title>

    <!-- Navigation starts -->
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="<?= site_url('Admin/index/') ?>">OOK-Clothing Admin</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                <a class="nav-link" href="<?=site_url('admin/index/')?>">Categories</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?=site_url('item/index/')?>">Items</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?=site_url('orders/index/')?>">Orders</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?=site_url('customer/index/')?>">Customers</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?=site_url('admin/admins/')?>">Admins</a>
                </li>
            </ul>
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="<?= site_url('/') ?>">Back to frontpage</a>
                </li>
                <?php 
                if (isset($_SESSION['adminname'])) {
                ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?= site_url('adminlogin/logout/') ?>">Logout</a>
                </li>
                <?php
                }
                ?>
            </ul>
        </div>
    </nav>
    <!-- Navigation ends -->
</head>

<body class="d-flex flex-column min-vh-100">
    <div class="container admin-puoli">