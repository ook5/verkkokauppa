</div>
<footer class="mt-auto p-1">
    <div class="container-fluid mt-2">
        <div class="row text-center">
            <div class="col mt-4">
                <h5>Contact info</h5>
                <h6>Email:</h6>
                <p>OOK.Clothing@jeemail.com</p>
                <h6>Phone:</h6>
                <p>+35666777666</p>
            </div>
            <div class="col">
                <img class="img-fluid" src="/img/footerkuva2.jpg"></img>
            </div>
        </div>
    </div>
</footer>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>