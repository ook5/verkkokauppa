<h1 class="frontpageh1">Welcome to OOK Clothing store!</h1>
<p>You can find our super comfy clothes from these categories:</p>

<div class="row justify-content-center">
<?php foreach($categories as $category): ?>
    <div class="card text">
        <a href="<?= site_url('store/index/' . $category['id'])?>">
            <h4><?=$category['catname']?></h4>
            <img src="<?= base_url('img/' . $category['catimage'])?>" class="img-fluid imgfront"></img>
        </a>
    </div>
<?php endforeach;?>

    <!-- <h4 id="fp">Look at this brand new and beautiful set of style:</h4>
    <div class="row justify-content-center">
        <div class="card">
            <a href="<?= site_url('store/item/1')?>">
                <h4>t-shirt1</h4>
                <p>49 €</p>
                <img src="<?= base_url('img/shirt01.png')?>"></img>
            </a>
        </div>
        <div class="card">
            <a href="<?= site_url('store/item/6')?>">
                <h4>pants1</h4>
                <p>139 €</p>
                <img src="<?= base_url('img/pants01.png')?>"></img>
            </a>
        </div>
        <div class="card">
            <a href="<?= site_url('store/item/12')?>">
                <h4>hat1</h4>
                <p>50 €</p>
                <img src="<?= base_url('img/hat01.png')?>"></img>
            </a>
        </div>
    </div> -->
</div>