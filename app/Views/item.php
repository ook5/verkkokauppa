<form method="post" action="<?= site_url('cart/add/' . $item['id']);?>">
    <div class="row justify-content-center">
        <div class="container-fluid">
            <img src="<?= base_url('img/' . $item['image'])?>" alt="" class="img-fluid">
        </div>
        <div class="container-fluid mt-4">
            <h4><?= $item['item'];?></h4>
            <p><?= $item['description'];?></p>
            <p class="price"><?= $item['price'];?> €</p>
            <button onclick="shoppingcart.classList.add('animate__animated', 'animate__swing');" class="btn btn-primary">Add to cart</button>
        </div>
    </div>
</form>