<div class="container-fluid">
<div class="row justify-content-center">
    <h1 class="frontpageh1">About us</h1>
    <div>
    <p>OOK Clothing is a webstore about ecological street wear. Most of our products are made
        from recycled materials. If you like comfy durable clothes then this is your store!</p>
    <p>There are three owls behind the OOK Clothing. They hope that you live long and prosper \\//!</p>
    </div>
    <div class="col-md-4 map-responsive">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1707.6206349828576!2d24.49963771633004!3d64.65964138382981!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4680590b5bc80393%3A0x48336fbb3d009c22!2sR%C3%A4ns%C3%A4rintie%2028%2C%2092120%20Raahe!5e0!3m2!1sen!2sfi!4v1605548654085!5m2!1sen!2sfi" width="350" height="225" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </div>
</div>
</div>