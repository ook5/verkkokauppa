<form action="/adminlogin/adminRegistration" method="post">
<h2>Register as Admin</h2>
    <div class="container-fluid">
        <?= \Config\Services::validation()->listErrors(); ?>
        <div class="form-row justify-content-center my-1">
            <div class="form-group col-12">
                <label>Admin name</label>
                <input class="form-control" name="adminname" placeholder="Admin name..." maxlength="50">
            </div>
            <div class="form-group col-6">
                <label>First name</label>
                <input class="form-control" name="firstname" placeholder="First name..." maxlength="50">
            </div>
            <div class="form-group col-6">
                <label>Last name</label>
                <input class="form-control" name="lastname" placeholder="Last name..." maxlength="50">
            </div>
            <div class="form-group col-12">
                <label>Password</label>
                <input class="form-control" name="password" type="password" placeholder="Password..." maxlength="255">
            </div>
            <div class="form-group col-12">
                <label>Confirm password</label>
                <input class="form-control" name="confirmpassword" type="password" placeholder="Confirm password..." maxlength="255">
            </div>
            <div class="form-group col-12 my-2">
                <button class="btn btn-primary">Submit</button>
            </div>
        </div>
    </div>
</form>