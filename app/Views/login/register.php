<form action="/login/registration" method="post">
<h2>Registration form</h2>
    <div class="container-fluid">
        <?= \Config\Services::validation()->listErrors(); ?>
        <div class="form-row justify-content-center my-1">
            <div class="col-4">
                <p>First name:<input class="form-control" name="firstname" placeholder="First name..." maxlength="50"></p>
            </div>
            <div class="col-4">
                <p>Last name:<input class="form-control" name="lastname" placeholder="Last name..." maxlength="50"></p>
            </div>
            <div class="col-4">
                <p>Email:<input class="form-control" name="email" placeholder="Email..." maxlength="100"></p>
            </div>
        </div>
        <div class="form-row justify-content-center my-1">
            <div class="col-12 my-1">
                <p>Address:<input class="form-control" name="address" placeholder="Address..." maxlength="100"></p>
            </div>
            <div class="col-4 my-1">
                <p>Postal code:<input class="form-control" name="postalcode" placeholder="Postal code..." maxlength="5"></p>
            </div>
            <div class="col-4 my-1">
                <p>City:<input class="form-control" name="city" placeholder="City..." maxlength="50"></p>
            </div>
            <div class="col-4 my-1">
                <p>Phone:<input class="form-control" name="phone" placeholder="Phonenumber..." maxlength="25"></p>
            </div>
            
        </div>
        <div class="form-row justify-content-center my-1">
            <div class="col-6">
                <p>Password:<input class="form-control" name="password" type="password" placeholder="Password..." maxlength="255"></p>
            </div>
            <div class="col-6 mb-1">
                <p>Confirm password:<input class="form-control" name="confirmpassword" type="password" placeholder="Confirm password..." maxlength="255"></p>
            </div>
            <div class="pb-1 my-1">
                <button class="btn btn-primary">Submit</button>
            </div>
        </div>
    </div>
</form>