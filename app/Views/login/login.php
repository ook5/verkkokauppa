<form action="/login/check" method="post">
    <h2>Login</h2>
    <div class="form-inline">
        <div class="col-12 d-flex justify-content-center">
            <?= \Config\Services::validation()->listErrors(); ?>
        </div>
        <div class="col-12 pb-1 d-flex justify-content-center">
            <input class="form-control" name="email" placeholder="Email..." maxlength="50">
        </div>
        <div class="col-12 pb-1 d-flex justify-content-center">
            <input class="form-control" name="password" type="password" placeholder="Password..." maxlength="255">
        </div>
        <div class="col-12 pb-1">
            <button class="btn btn-primary">Login</button>
            <button class="btn btn-secondary"><?= anchor('login/register', 'Register') ?></button>
        </div>
    </div>
    <div class="mt-5">
        <a href="<?= site_url('adminlogin/') ?>">
            <p>>>> Log in as an admin from here <<<</p>
        </a>
    </div>
</form>