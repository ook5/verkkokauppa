<form action="/adminlogin/check" method="post">
<h2>Login as Admin</h2>
    <div class="form-inline">
        <div class="col-12 d-flex justify-content-center">
            <?= \Config\Services::validation()->listErrors(); ?>
        </div>
        <div class="col-12 pb-1 d-flex justify-content-center">
            <input class="form-control" name="adminname" placeholder="Admin name..." maxlength="50">
        </div>
        <div class="col-12 pb-1 d-flex justify-content-center">
            <input class="form-control" name="password" type="password" placeholder="Password..." maxlength="255">
        </div>
        <div class="col-12 pb-1">
            <button class="btn btn-primary">Login </button>
        </div>
    </div>
</form>