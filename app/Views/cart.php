<div class="row justify-content-center">
    <div class="shoppingcart">
        <h4>Shopping cart</h4>
        <?php
        $total = 0;
        ?>
        <table class="table">
        <?php foreach ($items as $item): ?>
        <tr>
            <td class="text-left">
                <?= $item['item']?>
            </td>
            <td>
                <img src="<?= base_url('img/' . $item['image'])?>" alt="" class="imgcart">
            </td>
            <td>
                <?= $item['price'] . ' €'?>
            </td>
            <td>
                <?= $item['amount']?>
            </td>
            <td>
                <a class="cart_remove" href="<?= site_url('cart/remove/' . $item['id'])?>">
                    <i class="fas fa-minus-circle"></i>
                </a>
            </td>
        </tr>
        <?php
        $total += $item['price'] * $item['amount'];
        ?>
        <?php endforeach;?>
        <tr>
        <td></td>
        <td><strong>Total: <?php printf("%.2f €",$total);?></strong></td>
        <td></td>
        <td></td>
        <td>   
            <a id="clear" href="<?= site_url('cart/clear');?>">
            <i class="fas fa-trash"></i>
            </a>
        </td>
        </tr>
        </table>
        <h4>Customer info</h4>
        <form action="<?= site_url('cart/orders')?>" method="post">
        <div class="form-row justify-content-center my-1">
            <div class="form-group col-6 my-1">
                <label>First name</label>
                <input name="firstname" maxlength="50" class="form-control" value="<?= isset($_SESSION["user"]) ? $customer->firstname . '" readonly="' : "" ?>"/>
            </div>
            <div class="form-group col-6 my-1">
                <label>Last name</label>
                <input name="lastname" maxlength="50" class="form-control" value="<?= isset($_SESSION["user"]) ? $customer->lastname . '" readonly="' : "" ?>"/>
            </div>
            <div class="form-group col-6 my-1">
                <label>Address</label>
                <input name="address" maxlength="50" class="form-control" value="<?= isset($_SESSION["user"]) ? $customer->address . '" readonly="' : "" ?>"/>
            </div>
            <div class="form-group col-6 my-1">
                <label>Postalcode</label>
                <input name="postalcode" maxlength="50" class="form-control" value="<?= isset($_SESSION["user"]) ? $customer->postalcode . '" readonly="' : "" ?>"/>
            </div>
            <div class="form-group col-6 my-1">
                <label>City</label>
                <input name="city" maxlength="50" class="form-control" value="<?= isset($_SESSION["user"]) ? $customer->city . '" readonly="' : "" ?>"/>
            </div>
            <div class="form-group col-6 my-1">
                <label>Phone number</label>
                <input name="phone" maxlength="50" class="form-control" value="<?= isset($_SESSION["user"]) ? $customer->phone . '" readonly="' : "" ?>"/>
            </div>
            
            <div class="form-group col-12 my-1 p-3">
                <button class="btn btn-primary">Order</button>
            </div>
            </form>
        </div>
    </div>
</div>
