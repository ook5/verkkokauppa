<?php

use App\Libraries\Util;
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <h3>Your info</h3>
        </div>
        <div class="col-12 d-flex justify-content-center">
            <?= \Config\Services::validation()->listErrors(); ?>
        </div>
        <input type="hidden" name="id" value="<?= $customer->id ?>">

        <div class="col-6">
            <p>Firstname: <input readonly class="form-control" value="<?= $customer->firstname ?>"></p>
        </div>
        <div class="col-6">
            <p>Lastname: <input readonly class="form-control" value="<?= $customer->lastname ?>"></p>
        </div>
        <div class="col-12">
            <p>Address: <input readonly class="form-control" value="<?= $customer->address ?>"></p>
        </div>
        <div class="col-4">
            <p>Postalcode:<input readonly class="form-control" value="<?= $customer->postalcode ?>"></p>
        </div>
        <div class="col-4">
            <p>City:<input readonly class="form-control" value="<?= $customer->city ?>"></p>
        </div>
        <div class="col-4">
            <p>Phonenumber:<input readonly class="form-control" value="<?= $customer->phone ?>"></p>
        </div>
        <div class="col-6">
            <p>Email:<input readonly class="form-control" value="<?= $customer->email ?>"></p>
        </div>
        <div class="col-6">
            <p>Password:<input readonly class="form-control" value="<?= $customer->password ?>" type="password"></p>
        </div>
        <div class="col-12 justify-content-center">
        <div class="pb-1 my-1">
            <a href="<?= site_url('info/editInfo/') ?>"><button class="btn btn-primary">Edit your info</button></a>
        </div>
</div>
    </div>
    <div class="row mb-3">
        <div class="col-12">
            <h3>Your orders</h3>
        </div>
        <div class="col-12">
            <table class="col-12">
                <tr>
                    <th>Date and Time</th>
                    <th>Item</th>
                    <th>Amount</th>
                    <th>Price / item</th>
                </tr>
                <?php
                //$customer_id = $_SESSION['user'];
                $orders_id = 0;                
                ?>
                <?php foreach ($allorders as $orders) : ?>
                    <?php if ($customer->id != $orders['customer_id']) continue; ?>
                    <tr>
                        <?php if ($orders_id = $orders['ordersid']) { ?>
                            <td><?= Util::sqlDateToFi($orders['time']) ?></td>


                        <?php } else { ?>
                            <td></td>

                        <?php } ?>
                        <td><?= $orders['item'] ?></td>
                        <td><?= $orders['amount'] ?></td>
                        <td><?= $orders['price'] ?> €</td>

                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>