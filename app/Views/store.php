<div class="row justify-content-center mb-3">
<?php foreach($items as $item): ?>
    <div class="card">
        <a href="<?= site_url('store/item/' . $item['id'])?>">
            <h4><?=$item['item']?></h4>
            <p><?=$item['price']?> €</p>
            <img src="<?= base_url('img/' . $item['image'])?>" class="imgprev"></img>
        </a>
    </div>
<?php endforeach;?>
</div>