<?= \Config\Services::validation()->listErrors(); ?>
<h3><?= $titlecat?></h3>
<div>
    <a class="cat_add" href="<?= site_url('admin/savecat')?>">
        <i class="fas fa-plus-circle"> Add new category</i>
    </a>
</div>
<table class="table">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Image name</th>
        <th></th>
    </tr>
    <?php foreach($categories as $category): ?>
        <tr>
            <td><?= $category['id']?></td>
            <td><?= $category['catname']?></td>
            <td><?= $category['catimage']?></td>
            <td><?= anchor('admin/saveCat/' . $category['id'],'Edit')?></td>
            <td><a href="<?= site_url('admin/removecat/'. $category['id'])?>" onclick="return confirm('Are you sure you want to remove category and all items linked to it?')">Delete</a></td>
        </tr>
    <?php endforeach;?>
</table>