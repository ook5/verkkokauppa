<?php
use App\Libraries\Util;
?>
<h3>Here you can view/remove orders made by customers.</h3>
<table class="table">
    <tr>
        <th>Id</th>
        <th>Date</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Item</th>
        <th>Amount</th>
        <th></th>
    </tr>
<?php
$orders_id = 0;
?>
<?php foreach($allorders as $orders): ?>
<tr>
    <?php if ($orders_id != $orders['ordersid']) {?>
    <td><?=$orders['ordersid']?></td> 
    <td><?=Util::sqlDateToFi($orders['time'])?></td> 
    <td><?=$orders['firstname']?></td>
    <td><?=$orders['lastname']?></td>
    <?php } else { ?>
        <td></td><td></td><td></td><td></td>
    <?php } ?>

    <td><?=$orders['item']?></td>
    <td><?=$orders['amount']?></td>

    <?php if ($orders_id != $orders['ordersid']) {?>
        <td><a href="<?= site_url('orders/remove/'. $orders['ordersid'])?>" onclick="return confirm('Do you really want to remove the order?');">Remove</a></td>
    <?php } else {?>
        <td></td>
    <?php } ?>
    <?php
    $orders_id = $orders['ordersid'];
    ?>
</tr>
<?php endforeach;?>
</table>