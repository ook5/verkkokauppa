<h3>Here you can view/remove customers.</h3>
<table class="table">
    <tr>
        <th>Id</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Address</th>
        <th>Postal code</th>
        <th>City</th>
        <th>Phone</th>
        <th>Email</th>
        <th></th>
    </tr>
<?php
$customer_id = 0;
?>
<?php foreach($customers as $customer): ?>
<tr>
    <?php if ($customer_id != $customer['customerid']) {?>
    <td><?=$customer['customerid']?></td> 
    <td><?=$customer['firstname']?></td>
    <td><?=$customer['lastname']?></td>
    <td><?=$customer['address']?></td>
    <td><?=$customer['postalcode']?></td>
    <td><?=$customer['city']?></td>
    <td><?=$customer['phone']?></td>
    <td><?=$customer['email']?></td>
    <?php } else { ?>
        <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
    <?php } ?>

    <?php if ($customer_id != $customer['customerid']) {?>
        <td><a href="<?= site_url('customer/remove/'. $customer['customerid'])?>" onclick="return confirm('Do you really want to remove the customer?');">Remove</a></td>
    <?php } else {?>
        <td></td>
    <?php } ?>
    <?php
    $customer_id = $customer['customerid'];
    ?>
</tr>
<?php endforeach;?>
</table>