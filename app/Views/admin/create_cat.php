<h3><?= $title ?></h3>
<form action="/admin/saveCat" method="post">
    <div class="col-12">
        <?= Config\Services::validation()->listErrors(); ?>
        <input type="hidden" name="id" value="<?= $id?>">
        <div class="form-group">
            <label>Category name</label>
            <input class="form-control" name="catname" placeholder="Enter category name" value="<?= $catname?>" maxlength="255">
        </div>
        <div class="form-group">
            <label>Image (name)</label>
            <input class="form-control" name="catimage" placeholder="Enter image name" value="<?= $catimage?>" maxlength="100">
        </div>
        <button class="btn btn-primary">Save</button>
        <?= anchor('admin/index','Return') ?>
    </div>
</form>