<?= \Config\Services::validation()->listErrors(); ?>
<h3><?= $titleitem?></h3>
<div>
    <a class="item_add" href="<?= site_url('item/saveitem')?>">
        <i class="fas fa-plus-circle"> Add new item</i>
    </a>
</div>
<table class="table">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Price</th>
        <th>Category id</th>
        <th>Amount instore</th>
        <th>Description</th>
        <th>Image name</th>
        <th></th>
    </tr>
    <?php foreach($items as $item): ?>
        <tr>
            <td><?= $item['id']?></td>
            <td><?= $item['item'] ?></td>
            <td><?= $item['price'] ?></td>
            <td><?= $item['category_id'] ?></td>
            <td><?= $item['instore'] ?></td>
            <td><?= $item['description'] ?></td>
            <td><?= $item['image'] ?></td>
            <td><?= anchor('item/saveitem/' . $item['id'],'Edit')?></td>
            <td><a href="<?= site_url('item/removeitem/'. $item['id'])?>" onclick="return confirm('Are you sure you want to remove the item?')">Delete</a></td>
        </tr>
    <?php endforeach;?>
</table>