<h3>Here you can view/remove admins.</h3>
<?= anchor('adminlogin/adminRegister', 'Register new admin') ?>
<table class="table">
    <tr>
        <th>Id</th>
        <th>Admin name</th>
        <th>First name</th>
        <th>Last name</th>
        <th></th>
    </tr>
<?php
$admin_id = 0;
?>
<?php foreach($adminlogins as $adminlogin): ?>
<tr>
    <?php if ($admin_id != $adminlogin['adminloginid']) {?>
    <td><?=$adminlogin['adminloginid']?></td>
    <td><?=$adminlogin['adminname']?></td>
    <td><?=$adminlogin['firstname']?></td>
    <td><?=$adminlogin['lastname']?></td>
    <?php } else { ?>
        <td></td><td></td><td></td><td></td>
    <?php } ?>

    <?php if ($admin_id != $adminlogin['adminloginid']) {?>
        <td><a href="<?= site_url('admin/remove/'. $adminlogin['adminloginid'])?>" onclick="return confirm('Do you really want to remove the admin?');">Remove</a></td>
    <?php } else {?>
        <td></td>
    <?php } ?>
    <?php
    $admin_id = $adminlogin['adminloginid'];
    ?>
</tr>
<?php endforeach;?>
</table>