<h2><?= $title ?></h2>
<form action="/item/saveitem" method="post">
    <div class="col-12">
        <?= Config\Services::validation()->listErrors(); ?>
        <input type="hidden" name="id" value="<?= $id?>">
        <div class="form-group">
            <label>Product name</label>
            <input class="form-control" name="item" placeholder="Enter product name" value="<?= $item?>" maxlength="100">
        </div>
        <div class="form-group">
            <label>Product price</label>
            <input class="form-control" name="price" placeholder="Enter product price" value="<?= $price?>" maxlength="100">
        </div>
        <div class="form-group">
            <label>Category id</label>
            <input class="form-control" name="category_id" placeholder="Enter category id" value="<?= $category_id?>" maxlength="100">
        </div>
        <div class="form-group">
            <label>Amount instore</label>
            <input class="form-control" name="instore" placeholder="Enter amount instore" value="<?= $instore?>" maxlength="100">
        </div>
        <div class="form-group">
            <label>Description</label>
            <textarea class="form-control" name="description" placeholder="Enter product description"><?= $description?></textarea>
        </div>
        <div class="form-group">
            <label>Image (name)</label>
            <input class="form-control" name="image" placeholder="Enter image (name)" value="<?= $image?>" maxlength="100">
        </div>
        <button class="btn btn-primary">Save</button>
        <?= anchor('item/index','Return') ?>
    </div>
</form>