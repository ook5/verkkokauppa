<form action="/info/editInfo" method="post">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h2>Edit your info</h2>
            </div>
            <div class="col-12 d-flex justify-content-center">
                <?= \Config\Services::validation()->listErrors(); ?>
            </div>
            <input type="hidden" name="id" value="<?= $customer->id ?>">

            <div class="col-6">
                <p>Firstname: <input class="form-control" name="firstname" value="<?= $customer->firstname ?>"></input></p>
            </div>
            <div class="col-6">
                <p>Lastname: <input class="form-control" name="lastname" value="<?= $customer->lastname ?>"></input></p>
            </div>
            <div class="col-12">
                <p>Address: <input class="form-control" name="address" value="<?= $customer->address ?>"></input></p>
            </div>
            <div class="col-4">
                <p>Postalcode:<input class="form-control" name="postalcode" value="<?= $customer->postalcode ?>"></input></p>
            </div>
            <div class="col-4">
                <p>City:<input class="form-control" name="city" value="<?= $customer->city ?>"></input></p>
            </div>
            <div class="col-4">
                <p>Phonenumber:<input class="form-control" name="phone" value="<?= $customer->phone ?>"></input></p>
            </div>
        </div>
        <div class="form-row justify-content-center my-1">
            <div class="col-4">
                <p>Email:<input class="form-control" name="email" value="<?= $customer->email ?>"></input></p>
            </div>
            <div class="col-4">
                <p>Password:<input class="form-control" name="password" value="<?= $customer->password ?>" type="password"></input></p>
            </div>
            <div class="col-4">
                <p>Confirm password:<input class="form-control" name="confirmpassword" value="<?= $customer->password ?>" type="password"></input></p>
            </div>
            <div class="pb-1 my-1">
                <button class="btn btn-primary">Confirm</button>
            </div>
        </div>
    </div>
</form>