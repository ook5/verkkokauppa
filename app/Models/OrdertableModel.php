<?php  namespace App\Models;

use CodeIgniter\Model;


class OrdertableModel extends Model {
    protected $table = 'ordertable';

    protected $allowedFields = ['orders_id','item_id','amount'];


    public function removeByOrder($orders_id) {
        $this->where('orders_id',$orders_id);
        $this->delete();
    }
}