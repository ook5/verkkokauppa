<?php  namespace App\Models;

use CodeIgniter\Model;

class CustomerModel extends Model {
    protected $table = 'customer';
    protected $allowedFields = ['firstname','lastname','address','postalcode','city','phone','email','password'];


    public function getCustomers() {
        $this->select('customer.id as customerid,
        customer.firstname as firstname,
        customer.lastname as lastname,
        customer.address as address,
        customer.postalcode as postalcode,
        customer.city as city,
        customer.phone as phone,
        customer.email as email');
        $query = $this->get();
        return $query->getResultArray();
    }


    public function remove($id) {
        $this->where('id',$id);
        $this->delete();
    }

    public function getWithID($id) {
        return $this->getWhere(['id' => $id])->getRow();
    }
}