<?php namespace App\Models;

use CodeIgniter\Model;

class CatModel extends Model {
    protected $table = 'category';
    protected $allowedFields = ['catname','catimage'];

    public function getCategories() {
        return $this->findAll();
    }


    public function get($id) {
        return $this->getWhere(['id' => $id])->getRowArray();
    }


    public function remove($id) {
        $this->where('id',$id);
        $this->delete();
    }
}