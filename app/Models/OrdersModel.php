<?php  namespace App\Models;

use CodeIgniter\Model;


class OrdersModel extends Model {
    protected $table = 'orders';

    protected $allowedFields = ['customer_id'];


    public function getOrders() {
        $this->select('customer.firstname as firstname,
        customer.lastname as lastname,
        customer.id as customer_id,
        orders.id as ordersid,
        orders.time,
        ordertable.amount,
        item.item,
        item.price');      
        $this->join('customer','customer.id = orders.customer_id');
        $this->join('ordertable','ordertable.orders_id = orders.id');
        $this->join('item','item.id = ordertable.item_id');
        $query = $this->get();
        return $query->getResultArray();
    }


    public function remove($id) {
        $this->where('id',$id);
        $this->delete();
    }

}