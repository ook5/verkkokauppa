<?php  namespace App\Models;

use CodeIgniter\Model;

class AdminModel extends Model {
    protected $table = 'adminlogin';
    protected $allowedFields = ['id','adminname','firstname','lastname','password'];


    public function getAdminlist() {
        $this->select('adminlogin.id as adminloginid,
        adminlogin.adminname as adminname,
        adminlogin.firstname as firstname,
        adminlogin.lastname as lastname,
        adminlogin.password as password');
        $query = $this->get();
        return $query->getResultArray();
    }


    public function remove($id) {
        $this->where('id',$id);
        $this->delete();
    }

}