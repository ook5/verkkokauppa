<?php

namespace App\Models;

use CodeIgniter\Model;

class AdminLoginModel extends Model {
    protected $table = 'adminlogin';
    
    protected $allowedFields = ['adminname','firstname','lastname','password'];
    
    public function check($username, $password) {
        $this->where('adminname',$username); // Create where part to the select.
        $query = $this->get(); // Execute select (with where part defined).
        // print $this->getLastQuery(); // Might be used for debuggin.
        $row = $query->getRow(); // Get row.
        if ($row) { // If row was returned based on username...
            // Check if secured password is equal with password entered by user.
            if (password_verify($password,$row->password)) {
                return $row; // Return row (user object)
            }
        }
        return null; // Return null, because username and/or password is incorrect.
    }
}