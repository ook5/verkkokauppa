<?php namespace App\Models;

use CodeIgniter\Model;

class ItemModel extends Model {
    protected $table = 'item';
    protected $allowedFields = ['item','price','category_id','instore','description','image'];


    public function getWithCategory($category_id) {
        return $this->getWhere(['category_id' => $category_id])->getResultArray();
    }


    public function getItemsAdmin() {
        return $this->findAll();
    }


    public function getItem($id) {
        $this->where('id',$id);
        $query = $this->get();
        $item = $query->getRowArray();
        return $item;
    }


    public function getWithID($id) {
        return $this->getWhere(['id' => $id])->getRowArray();
    }


    public function remove($id) {
        $this->where('id',$id);
        $this->delete();
    }


    public function removeByCategory($category_id) {
        $this->where('category_id',$category_id);
        $this->delete();
    }
}