<?php

namespace App\Models;

use CodeIgniter\Model;

class InfoModel extends Model {
    protected $table = 'customer';
    protected $table1 = 'orders';
    protected $table2 = 'item';
    
    protected $allowedFields = ['id','password','firstname','lastname','address','postalcode','city','phone','email','item','price','timestamp'];
    
    public function check($username, $password) {
        $this->where('email', $username); // Create where part to the select.
        $query = $this->get(); // Execute select (with where part defined).
        // print $this->getLastQuery(); // Might be used for debuggin.
        $row = $query->getRow(); // Get row.
        if ($row) { // If row was returned based on username...
            // Check if secured password is equal with password entered by user.
            if (password_verify($password,$row->password)) {
                return $row; // Return row (user object)
            }
        }
        return null; // Return null, because username and/or password is incorrect.
    }

    public function getWithID($id) {
        return $this->getWhere(['id' => $id])->getRowArray();
    }
}