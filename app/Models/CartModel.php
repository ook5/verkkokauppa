<?php namespace App\Models;

use CodeIgniter\Model;

class CartModel extends Model {

    private $itemModel = null;
    private $customerModel = null;
    private $ordersModel = null;
    private $ordertableModel = null;

    function __construct()
    { 
        $session = \Config\Services::session();
        $session->start();
        if (!isset($_SESSION['cart'])) {
            $_SESSION['cart'] = array();
        }

        $this->db = \Config\Database::connect();

        $this->itemModel = new ItemModel();
        $this->customerModel = new CustomerModel();
        $this->ordersModel = new OrdersModel();
        $this->ordertableModel = new OrdertableModel();
    }


    public function cart() {
        return $_SESSION['cart'];
    }


    public function amount() {
        return count($_SESSION['cart']);
    }


    public function orders($customer) {
        $this->db->transStart(); 
        $this->customerModel->save($customer);
        if ($customer['id'] == 0) {
            $customer_id = $this->insertID();
        } else {
            $customer_id = $customer['id'];
        }
        
        $this->ordersModel->save(['customer_id' => $customer_id]);
        $orders_id = $this->insertID();
        foreach ($_SESSION['cart'] as $item) {
            $this->ordertableModel->save([
                'orders_id' => $orders_id,
                'item_id' => $item['id'],
                'amount' => $item['amount']
            ]);
        }
        $this->clear();
        $this->db->transComplete();
    }


    public function add($item_id) {
        $item = $this->itemModel->getItem($item_id);
		$cartItem['id'] = $item['id'];
		$cartItem['item'] = $item['item'];
		$cartItem['price'] = $item['price'];
		$cartItem['amount'] = 1;
        $cartItem['image'] = $item['image'];
		$this->addItemToArray($cartItem,$_SESSION['cart']);
    }


    public function remove($item_id) {
        for ($i = count($_SESSION['cart'])-1; $i >= 0;$i--) {
            $item = $_SESSION['cart'][$i];
            if ($item['id'] === $item_id) {
                array_splice($_SESSION['cart'], $i, 1);
            }
        }
    }


    public function clear() {
        $_SESSION['cart']= null;
        $_SESSION['cart'] = array();
    }


    private function addItemToArray($item,&$array) {
        for ($i = 0;$i < count($array);$i++ ) {
            if ($array[$i]['id'] === $item['id']) {
                $array[$i]['amount'] = $array[$i]['amount']  + 1;
                return;
            }
        }
        $item['amount'] = 1;
        array_push($array,$item);
    }

}